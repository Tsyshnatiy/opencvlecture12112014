#include <cv.h>
#include <highgui.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	CvCapture* capture = cvCreateCameraCapture(-1);
	
	int width = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);
	int height = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);
	
	IplImage *src;
	IplImage *dst = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	IplImage* gray = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	
	//cvNamedWindow("original", 1);
	cvNamedWindow("bin", 1);
	//cvNamedWindow("sub", 1);
	
	while (true)
	{
		src = cvQueryFrame(capture);
		cvCvtColor(src, gray, CV_RGB2GRAY);
		cvCanny(gray, dst, 50, 200, 3);

		cvShowImage("bin", dst);

		cvSub(gray, dst, dst);
		//cvShowImage("sub", dst);
		char c = cvWaitKey(33);
		if(c == 27) 
			break;
	}

	cvReleaseImage(&gray);
	cvReleaseImage(&dst);
	cvReleaseImage(&src);
	cvReleaseCapture(&capture);
	
	cvDestroyAllWindows();
	return 0;
}
