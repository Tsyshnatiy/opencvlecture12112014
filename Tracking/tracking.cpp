#include <cv.h>
#include <highgui.h>
#include <stdlib.h>
#include <stdio.h>

//R = 0
//G = 33 0.1294
//B = 103 0.4039
//MAX = 0.4039
//MIN = 0
//H = (60 * (-0.1294) / 0.4039 + 240) / 2 = 110
//S = 1 * 255
//V = 0.4039 * 255

IplImage* image = 0;
IplImage* dst = 0;
IplImage* hsv = 0;
IplImage* h_plane = 0;
IplImage* s_plane = 0;
IplImage* v_plane = 0;
IplImage* h_range = 0;
IplImage* s_range = 0;
IplImage* v_range = 0;
IplImage* hsv_and = 0;

int Hmin = 0;
int Hmax = 256;

int Smin = 0;
int Smax = 256;

int Vmin = 0;
int Vmax = 256;

int HSVmax = 256;

CvPoint getMassCenterGray(const IplImage* const image)
{
	int Xc = 0;
	int Yc = 0;
	int counter = 0;
	CvPoint result = cvPoint(0, 0);
	for(int y = 0; y < image->height; ++y)
	{
			uchar* ptr = (uchar*) (image->imageData + y * image->widthStep);
			for(int x = 0; x < image->width; ++x)
			{
					if(ptr[x] > 0)
					{
							Xc += x;
							Yc += y;
							counter++; 
					}
			}
	}
	
	if(counter != 0)
	{
			result = cvPoint(Xc / counter, Yc / counter);
	}
	
	return result;
}

int main(int argc, char* argv[])
{
	CvCapture* capture = cvCreateCameraCapture(-1);
	
	int width = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);
	int height = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);

	hsv = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 3);
	h_plane = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	s_plane = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	v_plane = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	h_range = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	s_range = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	v_range = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
	hsv_and = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);

	printf("Creating windows\n");
	cvNamedWindow("original",CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("H",CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("S",CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("V",CV_WINDOW_AUTOSIZE);
	cvNamedWindow("H range",CV_WINDOW_AUTOSIZE);
	cvNamedWindow("S range",CV_WINDOW_AUTOSIZE);
	cvNamedWindow("V range",CV_WINDOW_AUTOSIZE);
	cvNamedWindow("hsv and",CV_WINDOW_AUTOSIZE);
	
	printf("Starting capture\n");
	while (true)
	{
		image = cvQueryFrame(capture);

		cvCvtColor(image, hsv, CV_BGR2HSV); 
		cvCvtPixToPlane(hsv, h_plane, s_plane, v_plane, 0);

		double framemin = 0;
		double framemax = 0;

		Vmin = 60; //80
		Vmax = 200; //103
		Hmin = 60; //80
		Hmax = 140; //110
		Smin = 180; //200
		Smax = 255; //255
		cvInRangeS(v_plane, cvScalar(Vmin), cvScalar(Vmax), v_range);
		cvInRangeS(h_plane, cvScalar(Hmin), cvScalar(Hmax), h_range);
		cvInRangeS(s_plane, cvScalar(Smin), cvScalar(Smax), s_range);

		//cvShowImage( "H", h_plane );
		//cvShowImage( "S", s_plane );
		//cvShowImage( "V", v_plane );

		cvShowImage("H range", h_range);
		cvShowImage("S range", s_range);
		cvShowImage("V range", v_range);

		cvAnd(h_range, s_range, hsv_and);
		cvAnd(hsv_and, v_range, hsv_and);
		
		CvPoint center = getMassCenterGray(hsv_and);
		
		printf("Center: %3d %3d\n", center.x, center.y);
		cvLine(image, cvPoint(center.x, 0), cvPoint(center.x, image->height), CV_RGB(255,0,0), 3);
		cvLine(image, cvPoint(0, center.y), cvPoint(image->width, center.y), CV_RGB(255,0,0), 3);
		cvShowImage("hsv and", hsv_and );
		cvShowImage("original",image);

		char c = cvWaitKey(33);
		if (c == 27) 
		{
			break;
		}
		/*printf("\n[i] Results:\n");
		printf("[H] %d x %d\n", Hmin, Hmax);
		printf("[S] %d x %d\n", Smin, Smax);
		printf("[V] %d x %d\n", Vmin, Vmax);*/
	}
	
	cvReleaseImage(&image);
	cvReleaseImage(&hsv);
	cvReleaseImage(&h_plane);
	cvReleaseImage(&s_plane);
	cvReleaseImage(&v_plane);
	cvReleaseImage(&h_range);
	cvReleaseImage(&s_range);
	cvReleaseImage(&v_range);
	cvReleaseImage(&hsv_and);
	cvDestroyAllWindows();
	return 0;
}
