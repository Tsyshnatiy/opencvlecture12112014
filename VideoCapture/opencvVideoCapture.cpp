#include <cv.h>
#include <highgui.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv)
{
        cvNamedWindow("Capture", CV_WINDOW_AUTOSIZE);
        CvCapture* capture = cvCreateCameraCapture(-1);
        IplImage* frame;
 
        while(1) {
                frame = cvQueryFrame(capture);
                if(!frame) 
					break;
 
                cvShowImage("Capture", frame);
                char c = cvWaitKey(33);
                if(c == 27) 
					break;
        }
        cvReleaseCapture(&capture);
        cvDestroyWindow("Capture");
        return 0;
}
