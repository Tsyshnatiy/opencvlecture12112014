# README #
This repo contains code which is demonstrated during opencv lecture 12/11/2014.

### Demos ###
1. VideoCapture contains video capture from webcam in python and c.
2. Canny contains applying canny detector to image from webcam in c.
3. Tracking uses hsv for detecting objects by color. It detects my blue t-short.
4. HistogramsRGB shows rgb histogram of an input image.
5. Dft performs Discrete Fourier transform, shows image spectrum and restores image then.


If you think this code is useful for you, feel free to use it.